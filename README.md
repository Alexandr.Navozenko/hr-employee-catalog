# HR_Employee_List

### Requirements

* [Java 8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
* [Maven](https://maven.apache.org/download.cgi)
* [javaFX](https://docs.oracle.com/javafx/2/)
* [Spring Boot](https://spring.io/projects/spring-boot)

### Run

For run application we can use next command:
    
    mvn clean spring-boot:run


