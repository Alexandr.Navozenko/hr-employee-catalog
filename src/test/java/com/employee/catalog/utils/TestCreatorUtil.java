package com.employee.catalog.utils;

import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Position;

import java.util.ArrayList;
import java.util.List;

public class TestCreatorUtil {

    private static final String DEFAULT_FIRST_NAME = "John_";

    private static final String DEFAULT_LAST_NAME = "Doe_";

    public static Employee createEmployee(int prefixName) {
        Employee employee = new Employee(
                DEFAULT_FIRST_NAME + prefixName,
                DEFAULT_LAST_NAME + prefixName,
                new Position()
        );

        return employee;
    }

    public static List<Employee> createEmployees() {
        List<Employee> employees = new ArrayList<>();
        employees.add(createEmployee(1));
        employees.add(createEmployee(2));
        employees.add(createEmployee(3));
        employees.add(createEmployee(4));
        employees.add(createEmployee(5));

        return employees;
    }
}
