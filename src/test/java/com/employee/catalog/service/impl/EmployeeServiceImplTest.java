package com.employee.catalog.service.impl;

import com.employee.catalog.domain.Employee;
import com.employee.catalog.repository.EmployeeRepository;
import com.employee.catalog.service.EmployeeService;
import com.employee.catalog.utils.TestCreatorUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class EmployeeServiceImplTest {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public EmployeeService employeeService() {
            return new EmployeeServiceImpl();
        }
    }

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    private Employee employee;

    private List<Employee> employees;

    @BeforeEach
    public void setup() {
        this.employees = TestCreatorUtil.createEmployees();
        this.employee = employees.get(0);
    }

    @Test
    void findAll() {
        when(employeeRepository.findAll()).thenReturn(employees);
        List<Employee> actualEmployees = employeeService.findAll();

        assertArrayEquals(employees.toArray(), actualEmployees.toArray());
    }

    @Test
    void save() {
        when(employeeRepository.save(employee)).thenReturn(employee);
        Employee actualEmployee = employeeService.save(employee);

        assertNotNull(actualEmployee);
        assertEquals(employee, actualEmployee);
    }

    @Test
    void delete() {
        employeeRepository.delete(employee);

        verify(employeeRepository).delete(any());
    }
}
