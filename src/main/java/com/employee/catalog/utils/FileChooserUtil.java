package com.employee.catalog.utils;

import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;
import java.util.List;

public class FileChooserUtil {
    public static File getFilePath(Window ownerWindow,
                                   String defaultFileName,
                                   String description,
                                   List<String> extensions) {
        final FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialFileName(defaultFileName);
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter(description, extensions));

        return fileChooser.showSaveDialog(ownerWindow);
    }
}
