package com.employee.catalog.utils.employeeCV;

import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Education;
import com.employee.catalog.domain.Experience;
import com.employee.catalog.model.EmployeeReportModel;
import lombok.extern.slf4j.Slf4j;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.component.FillerBuilder;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.SubreportBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Set;

import static com.employee.catalog.utils.EntityUtil.getEmployeeFullName;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;

@Slf4j
public class BuildCVEmployee implements Runnable {

    private final Employee employee;

    private final boolean appendExperience;

    private final boolean appendEducation;

    private final File path;

    private static final String SUCCESSFULLY_MESSAGE = "Employee CV__%s pdf file saved successfully";

    private static final String DIVIDER = "_";

    private static final String SPACE = " ";

    private static final String DEFAULT_FULL_NAME = "Full Name: %s";

    private static final String DEFAULT_DEPARTMENT = "Department: %s";

    private static final String DEFAULT_POSITION = "Position: %s";

    private static final int VERTICAL_GAP = 50;

    private static final int DEFAULT_GAP = 0;

    private final StyleBuilder styleBold = stl.style().bold();

    private final StyleBuilder styleBackgroundColor = stl.style().setBackgroundColor(Color.LIGHT_GRAY);

    private final StyleBuilder styleBorder = stl.style()
            .setBorder(stl.pen1Point())
            .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);


    public BuildCVEmployee(Employee employee, boolean appendExperience, boolean appendEducation, File path) {
        this.employee = employee;
        this.appendExperience = appendExperience;
        this.appendEducation = appendEducation;
        this.path = path;
    }

    @Override
    public void run() {
        renderCVEmployee(appendEducation, appendExperience, path);
        final String successfullyMessage = String.format(SUCCESSFULLY_MESSAGE, getEmployeeFullName(employee, DIVIDER));
        log.info(successfullyMessage);
    }

    private void renderCVEmployee(boolean appendEducation, boolean appendExperience, File path) {
        render(path.getAbsolutePath(), new EmployeeReportModel(employee,
                getExperiencesOrEmptySet(appendExperience),
                getEducationsOrEmptySet(appendEducation)));
    }

    private Set<Education> getEducationsOrEmptySet(boolean appendEducation) {
        return appendEducation ? employee.getEducations() : Collections.emptySet();
    }

    private Set<Experience> getExperiencesOrEmptySet(boolean appendExperience) {
        return appendExperience ? employee.getExperiences() : Collections.emptySet();
    }

    public void render(String filePath, EmployeeReportModel employee) {
        try {
            final JasperReportBuilder report = report();
            report
                    .pageHeader(buildEmployeeData(employee))
                    .summary(
                            showVerticalGap(appendEducation),
                            buildEducationData(employee),
                            showVerticalGap(appendExperience),
                            buildExperienceData(employee)
                    )
                    .setDataSource(
                            new JRBeanCollectionDataSource(Collections.singletonList(employee))
                    )
                    .setPageFormat(PageType.A4)
                    .pageFooter(Components.pageXofY())
                    .toPdf(new FileOutputStream(filePath));
        } catch (Throwable throwable) {
            throw new RuntimeException("error while rendering report", throwable);
        }
    }

    private SubreportBuilder buildEducationData(EmployeeReportModel employee) {
        return cmp.subreport(report()
                .title(cmp.text("Education").setStyle(styleBackgroundColor))
                .columns(
                        col.column("Name", "educationName", type.stringType()).setStyle(styleBorder),
                        col.column("Date", "date", type.dateType()).setStyle(styleBorder),
                        col.column("Certificate Id", "certificate",
                                type.stringType()).setStyle(styleBorder)
                ))
                .setDataSource(new JRBeanCollectionDataSource(employee.getEducations()));
    }

    private SubreportBuilder buildExperienceData(EmployeeReportModel employee) {
        return cmp.subreport(report()
                .title(cmp.text("Experience").setStyle(styleBackgroundColor))
                .columns(
                        col.column("Position", "position", type.stringType()).setStyle(styleBorder),
                        col.column("From", "dateFrom", type.dateType()).setStyle(styleBorder),
                        col.column("To", "dateTo", type.dateType()).setStyle(styleBorder)
                )
                .setDataSource(new JRBeanCollectionDataSource(employee.getExperiences())));
    }

    private HorizontalListBuilder buildEmployeeData(EmployeeReportModel employee) {
        final String FullName = String.format(DEFAULT_FULL_NAME,
                employee.getFirstName() + SPACE +  employee.getLastName());
        final String department = String.format(DEFAULT_DEPARTMENT, employee.getDepartment().getName());
        final String position = String.format(DEFAULT_POSITION, employee.getPosition().getName());

        return cmp.horizontalList(
                cmp.verticalList(
                        cmp.text(FullName).setStyle(styleBold),
                        cmp.text(department).setStyle(styleBold),
                        cmp.text(position).setStyle(styleBold)
                ).setStyle(styleBold)
        ).setFixedWidth(400).setFixedHeight(50);
    }

    private FillerBuilder showVerticalGap(boolean showingVerticalGap) {
        final int heightGap = showingVerticalGap ? VERTICAL_GAP : DEFAULT_GAP;
        return cmp.verticalGap(heightGap);
    }
}
