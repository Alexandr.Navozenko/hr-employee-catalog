package com.employee.catalog.utils;

import java.sql.Date;
import java.time.Instant;

public class DateUtil {
    public static Date getCurrentDate() {
        return new Date(Instant.now().toEpochMilli());
    }
}
