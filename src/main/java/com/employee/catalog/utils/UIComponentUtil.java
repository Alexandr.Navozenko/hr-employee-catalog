package com.employee.catalog.utils;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class UIComponentUtil {

    public static Image initializeButtonImage(String fontAwesomeIconName) {
        final FontAwesomeIconView iconView = new FontAwesomeIconView(FontAwesomeIcon.valueOf(fontAwesomeIconName));
        final WritableImage writableImg = iconView.snapshot(null, null);

        return SwingFXUtils.toFXImage(SwingFXUtils.fromFXImage(writableImg, null), null);
    }

    public static void showAlert(String alertType, String alertMessage) {
        new Alert(Alert.AlertType.valueOf(alertType), alertMessage).showAndWait();
    }

    public static Alert initializeConfirmationAlert(String alertMessage, String alertTitle) {
        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                alertMessage,
                ButtonType.CANCEL,
                ButtonType.OK);

        alert.setTitle(alertTitle);

        return alert;
    }
}
