package com.employee.catalog.utils;

import com.employee.catalog.domain.Employee;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class EntityUtil {

    public static String getEmployeeFullName(Employee employee, String divider) {
        return employee.getFirstName() + divider + employee.getLastName();
    }
}
