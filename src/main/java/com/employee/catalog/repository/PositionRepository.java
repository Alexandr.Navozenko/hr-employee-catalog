package com.employee.catalog.repository;


import com.employee.catalog.domain.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer> {
    Position findByName(String name);

    List<Position> findByDepartmentId(Integer id);
}
