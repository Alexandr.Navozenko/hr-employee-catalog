package com.employee.catalog.common;

import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Position;
import com.employee.catalog.utils.EntityUtil;
import javafx.beans.property.SimpleStringProperty;
import lombok.Getter;

@Getter
public class EmployeeView {

    private static final String DIVIDER = " ";

    private final Employee employee;
    private final SimpleStringProperty fullName;
    private final SimpleStringProperty position;
    private final SimpleStringProperty department;

    public EmployeeView(Employee employee) {
        final Position position = employee.getPosition();
        
        this.employee = employee;
        this.fullName = new SimpleStringProperty(EntityUtil.getEmployeeFullName(employee, DIVIDER));
        this.position = new SimpleStringProperty(position.getName());
        this.department = new SimpleStringProperty(position.getDepartment().getName());
    }
}
