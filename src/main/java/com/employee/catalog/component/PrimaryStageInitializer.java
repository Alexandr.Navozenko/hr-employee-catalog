package com.employee.catalog.component;

import javafx.stage.Stage;
import org.springframework.stereotype.Component;

@Component
public class PrimaryStageInitializer {

    private Stage primaryStage;

    public void initStage(Stage stage) {
        this.primaryStage = stage;
    }

    public Stage getPrimaryStage() {
        if (primaryStage == null) {
            throw new NullPointerException();
        }

        return primaryStage;
    }
}
