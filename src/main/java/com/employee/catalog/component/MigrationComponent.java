package com.employee.catalog.component;

import com.employee.catalog.domain.Department;
import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Position;
import com.employee.catalog.repository.DepartmentRepository;
import com.employee.catalog.repository.PositionRepository;
import com.employee.catalog.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MigrationComponent {

    private final EmployeeService employeeService;

    private final DepartmentRepository departmentRepository;

    private final PositionRepository positionRepository;

    private static final String FIRST_NAME_ONE = "Name1";
    private static final String FIRST_NAME_TWO = "Name2";
    private static final String FIRST_NAME_THREE = "Name3";
    private static final String FIRST_NAME_FOUR = "Name4";
    private static final String FIRST_NAME_FIVE = "Name5";

    private static final String LAST_NAME_ONE = "SurName1";
    private static final String LAST_NAME_TWO = "SurName2";
    private static final String LAST_NAME_THREE = "SurName3";
    private static final String LAST_NAME_FOUR = "SurName4";
    private static final String LAST_NAME_FIVE = "SurName5";

    private static final String DEPARTMENT_ONE = "department1";
    private static final String DEPARTMENT_TWO = "department2";
    private static final String DEPARTMENT_THREE = "department3";

    private static final String POSITION_ONE_ONE = "Position_1-1";
    private static final String POSITION_ONE_TWO = "Position_1-2";
    private static final String POSITION_ONE_THREE = "Position_1-3";

    private static final String POSITION_TWO_ONE = "Position_2-1";
    private static final String POSITION_TWO_TWO = "Position_2-2";
    private static final String POSITION_TWO_THREE = "Position_2-3";

    private static final String POSITION_THREE_ONE = "Position_3-1";
    private static final String POSITION_THREE_TWO = "Position_3-2";
    private static final String POSITION_THREE_THREE = "Position_3-3";

    @Autowired
    public MigrationComponent(EmployeeService employeeService,
                              DepartmentRepository departmentRepository,
                              PositionRepository positionRepository) {
        this.employeeService = employeeService;
        this.departmentRepository = departmentRepository;
        this.positionRepository = positionRepository;
    }

    @PostConstruct
    public void createEmployee() {
        initDepartment();
        initPosition();
        initEmployee();
    }

    private void initEmployee() {
        final Position positionOne = positionRepository.findByName(POSITION_TWO_ONE);
        employeeService.save(new Employee(FIRST_NAME_ONE, LAST_NAME_ONE, positionOne));

        final Position positionTwo = positionRepository.findByName(POSITION_ONE_THREE);
        employeeService.save(new Employee(FIRST_NAME_TWO, LAST_NAME_TWO, positionTwo));

        final Position positionThree = positionRepository.findByName(POSITION_THREE_THREE);
        employeeService.save(new Employee(FIRST_NAME_THREE, LAST_NAME_THREE, positionThree));

        final Position positionFour = positionRepository.findByName(POSITION_TWO_THREE);
        employeeService.save(new Employee(FIRST_NAME_FOUR, LAST_NAME_FOUR, positionFour));

        final Position positionFive = positionRepository.findByName(POSITION_ONE_ONE);
        employeeService.save(new Employee(FIRST_NAME_FIVE, LAST_NAME_FIVE, positionFive));
    }

    private void initDepartment() {
        departmentRepository.save(new Department(DEPARTMENT_ONE));
        departmentRepository.save(new Department(DEPARTMENT_TWO));
        departmentRepository.save(new Department(DEPARTMENT_THREE));
    }

    private void initPosition() {
        Department department = departmentRepository.findByName(DEPARTMENT_ONE);
        positionRepository.save(new Position(POSITION_ONE_ONE, department));
        positionRepository.save(new Position(POSITION_ONE_TWO, department));
        positionRepository.save(new Position(POSITION_ONE_THREE, department));

        department = departmentRepository.findByName(DEPARTMENT_TWO);
        positionRepository.save(new Position(POSITION_TWO_ONE, department));
        positionRepository.save(new Position(POSITION_TWO_TWO, department));
        positionRepository.save(new Position(POSITION_TWO_THREE, department));

        department = departmentRepository.findByName(DEPARTMENT_THREE);
        positionRepository.save(new Position(POSITION_THREE_ONE, department));
        positionRepository.save(new Position(POSITION_THREE_TWO, department));
        positionRepository.save(new Position(POSITION_THREE_THREE, department));
    }
}
