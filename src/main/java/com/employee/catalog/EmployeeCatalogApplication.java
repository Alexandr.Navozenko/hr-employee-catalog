package com.employee.catalog;

import com.employee.catalog.component.PrimaryStageInitializer;
import com.employee.catalog.controller.EmployeeListController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class EmployeeCatalogApplication extends Application {

    private ConfigurableApplicationContext applicationContext;

    @Autowired
    private FxWeaver weaver;

    @Autowired
    private PrimaryStageInitializer primaryStageInitializer;

    @Override
    public void init() {
        applicationContext = SpringApplication.run(getClass());
        applicationContext.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void start(Stage stage) {
        primaryStageInitializer.initStage(stage);
        Scene scene = new Scene(weaver.loadView(EmployeeListController.class));
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() {
        this.applicationContext.close();
        Platform.exit();
    }

    public static void main(String[] args) {
        launch();
    }
}
