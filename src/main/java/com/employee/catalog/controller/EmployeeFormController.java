package com.employee.catalog.controller;

import com.employee.catalog.component.PrimaryStageInitializer;
import com.employee.catalog.controller.element.DateColumnTableCell;
import com.employee.catalog.controller.element.EducationCertificateColumnTableCell;
import com.employee.catalog.controller.element.EducationCertificateIdColumnTableCell;
import com.employee.catalog.controller.element.EducationNameColumnTableCell;
import com.employee.catalog.controller.element.ExperiencePositionColumnTableCell;
import com.employee.catalog.controller.element.TableColumnActionTableCell;
import com.employee.catalog.domain.Department;
import com.employee.catalog.domain.Education;
import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Experience;
import com.employee.catalog.service.DepartmentService;
import com.employee.catalog.service.EducationService;
import com.employee.catalog.service.EmployeeService;
import com.employee.catalog.service.ExperienceService;
import com.employee.catalog.service.PositionService;
import com.employee.catalog.utils.DateUtil;
import com.employee.catalog.utils.FileChooserUtil;
import com.employee.catalog.utils.EntityUtil;
import com.employee.catalog.utils.UIComponentUtil;
import com.employee.catalog.utils.employeeCV.BuildCVEmployee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import net.rgielen.fxweaver.core.FxControllerAndView;
import net.rgielen.fxweaver.core.FxmlView;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import java.io.File;
import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static javafx.stage.Modality.WINDOW_MODAL;

@Slf4j
@Component
@FxmlView("EmployeeForm.fxml")
public class EmployeeFormController {

    private Stage stage;

    private Employee employee;

    private ObservableList<Education> educations;

    private ObservableList<Experience> experiences;

    private String errorValidateFields = "";

    private static final String ERROR_MESSAGE_FIRST_NAME = "Field 'First Name' is empty\n";

    private static final String ERROR_MESSAGE_LAST_NAME = "Field 'Last Name' is empty\n";

    private static final String ERROR_MESSAGE_POSITION = "Field 'Position' is empty\n";

    private static final String TITLE_EMPLOYEE_FORM = "Employee Form";

    private static final String EMPTY_TEXT = "";

    private static final String ERROR = "ERROR";

    private static final String INFORMATION = "INFORMATION";

    private static final String ADD_NEW_ROW_BUTTON_LABEL = "+";

    private static final String SUCCESS_MESSAGE = "%s, Employee saved successfully";

    private static final String SPACE = " ";

    private static final String DIVIDER = "_";

    private static final double VBOX_SPACING = 20.0;

    private static final double DIALOG_WINDOWS_WIDTH = 50.0;

    private static final String PREPARED_CV_BUTTON = "Prepare CV";

    private static final String CANCEL_BUTTON = "Cancel";

    private static final String INCLUDE_EDUCATION_TEXT = "Include 'Education'";

    private static final String INCLUDE_EXPERIENCE_TEXT = "Include 'Experience'";

    private static final String EXTENSIONS = "*.pdf";

    private static final String FILE_CHOOSER_DESCRIPTION = "Document-%s";

    private static final String DEFAULT_CV_FILE_NAME = "CV__%s.pdf";

    private final FxControllerAndView<EmployeeListController, BorderPane> employeeList;

    private final EmployeeService employeeService;

    private final DepartmentService departmentService;

    private final PositionService positionService;

    private final EducationService educationService;

    private final ExperienceService experienceService;

    private final PrimaryStageInitializer primaryStageInitializer;

    @FXML
    private AnchorPane dialog;

    @FXML
    private TextField fieldFirstName;

    @FXML
    private TextField fieldLastName;

    @FXML
    private ChoiceBox<String> selectDepartment;

    @FXML
    private ChoiceBox<String> selectPosition;

    @FXML
    private TableView<Education> educationTable;

    @FXML
    private TableColumn<Education, Button> educationColumnAction;

    @FXML
    private TableColumn<Education, String> educationColumnName;

    @FXML
    private TableColumn<Education, Date> educationColumnDate;

    @FXML
    private TableColumn<Education, Boolean> educationColumnCertification;

    @FXML
    private TableColumn<Education, String> educationColumnCertificationID;

    @FXML
    private TableView<Experience> experienceTable;

    @FXML
    private TableColumn<Experience, Button> experienceColumnAction;

    @FXML
    private TableColumn<Experience, String> experienceColumnPosition;

    @FXML
    private TableColumn<Experience, Date> experienceColumnDateFrom;

    @FXML
    private TableColumn<Experience, Date> experienceColumnDateTo;

    @Autowired
    public EmployeeFormController(FxControllerAndView<EmployeeListController, BorderPane> employeeList,
                                  EmployeeService employeeService,
                                  DepartmentService departmentService,
                                  PositionService positionService,
                                  EducationService educationService,
                                  ExperienceService experienceService,
                                  PrimaryStageInitializer primaryStageInitializer) {
        this.employeeList = employeeList;
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.positionService = positionService;
        this.educationService = educationService;
        this.experienceService = experienceService;
        this.primaryStageInitializer = primaryStageInitializer;
    }

    @FXML
    void initialize() {
        educations = FXCollections.observableArrayList();
        experiences = FXCollections.observableArrayList();

        initStage();

        initializeDepartments();
        initializePositions();

        initEmployeeFieldsListener();
        initEducationTable();
        initExperienceTable();
    }

    @FXML
    void prepareCV(ActionEvent event) {
        saveCurrentEmployee();
        initializePreparedCVDialog();
    }

    @FXML
    void saveDetailEmployee(ActionEvent event) {
        saveCurrentEmployee();
    }

    @FXML
    void closeEmployeeForm(ActionEvent event) {
        this.stage.close();
        employeeList.getController().refreshTableView();
    }

    public void showAndWait(Employee employee) {
        this.employee = employee;
        fillForm();
        this.stage.showAndWait();
    }

    private void saveCurrentEmployee() {
        if (validatedFields()) {
            employee.setEducations(removeBlankEducations(educations));
            employee.setExperiences(removeBlankExperiences(experiences));

            try {
                employeeService.save(employee);

                final String textMessage = String.format(SUCCESS_MESSAGE,
                        EntityUtil.getEmployeeFullName(employee, SPACE));
                UIComponentUtil.showAlert(INFORMATION, textMessage);
            } catch (TransactionSystemException error) {
                log.error(error.getMessage(), error);
                UIComponentUtil.showAlert(ERROR, error.getLocalizedMessage());
            }
        } else {
            log.error(errorValidateFields);
            UIComponentUtil.showAlert(ERROR, errorValidateFields);
            errorValidateFields = EMPTY_TEXT;
        }
    }

    private static Set<Education> removeBlankEducations(ObservableList<Education> educations) {
        return educations.stream()
                .filter(education -> !StringUtils.isBlank(education.getEducationName()))
                .collect(Collectors.toSet());
    }

    private static Set<Experience> removeBlankExperiences(ObservableList<Experience> experiences) {
        return experiences.stream()
                .filter(education -> !StringUtils.isBlank(education.getPosition()))
                .collect(Collectors.toSet());
    }

    private boolean validatedFields() {
        if (StringUtils.isBlank(fieldFirstName.getText())) {
            errorValidateFields = errorValidateFields.concat(ERROR_MESSAGE_FIRST_NAME);
        }

        if (StringUtils.isBlank(fieldLastName.getText())) {
            errorValidateFields = errorValidateFields.concat(ERROR_MESSAGE_LAST_NAME);
        }

        if (StringUtils.isBlank(selectPosition.getValue())) {
            errorValidateFields = errorValidateFields.concat(ERROR_MESSAGE_POSITION);
        }

        return errorValidateFields.isEmpty();
    }

    private void initStage() {
        this.stage = new Stage();
        this.stage.setTitle(TITLE_EMPLOYEE_FORM);
        this.stage.initStyle(StageStyle.TRANSPARENT);
        this.stage.initModality(WINDOW_MODAL);
        this.stage.initOwner(primaryStageInitializer.getPrimaryStage());
        this.stage.setScene(new Scene(dialog));
    }

    private void fillForm() {
        educations.clear();
        experiences.clear();

        if (this.employee == null) {
            this.employee = new Employee();
            selectPosition.getSelectionModel().clearSelection();
            selectDepartment.getSelectionModel().clearSelection();
        } else {
            final String positionName = this.employee.getPosition().getName();
            selectDepartment.setValue(this.employee.getPosition().getDepartment().getName());
            selectPosition.setValue(positionName);

            educations.addAll(this.employee.getEducations());
            experiences.addAll(this.employee.getExperiences());
        }

        fieldFirstName.setText(this.employee.getFirstName());
        fieldLastName.setText(this.employee.getLastName());
    }

    private void initializeDepartments() {
        selectDepartment.getItems().clear();
        final List<String> departmentsName = departmentService.getDepartmentNames();
        selectDepartment.getItems().addAll(departmentsName);
    }

    private void initializePositions() {
        selectDepartment.setOnAction(event -> {
            selectPosition.getItems().clear();
            selectPosition.setDisable(selectDepartment.isPressed());
            final Department department = findDepartmentByName(selectDepartment.getValue());

            if (department != null) {
                final List<String> positionNames = positionService.getPositionNames(department);
                selectPosition.getItems().addAll(positionNames);
            }
        });
    }

    private Department findDepartmentByName(String departmentName) {
        return departmentService.findByName(departmentName);
    }

    private void initEmployeeFieldsListener() {
        fieldFirstName.textProperty().addListener((observable, oldValue, newValue) -> employee.setFirstName(newValue));
        fieldLastName.textProperty().addListener((observable, oldValue, newValue) -> employee.setLastName(newValue));
        selectPosition.setOnAction(
                actionEvent -> employee.setPosition(positionService.findByName(selectPosition.getValue())));
    }

    private void initEducationTable() {
        educationTable.setItems(educations);
        educationTable.setEditable(true);

        final Button addNewRowButton = new Button(ADD_NEW_ROW_BUTTON_LABEL);
        addNewRowButton.setOnMouseClicked(event -> educations.add(new Education(employee, DateUtil.getCurrentDate())));

        educationColumnAction.setSortable(false);
        educationColumnAction.setGraphic(addNewRowButton);
        educationColumnAction.setCellFactory(
                model -> new TableColumnActionTableCell(revolveEducationDeleteRowAction()));

        educationColumnName.setCellFactory(param -> new EducationNameColumnTableCell());

        educationColumnDate.setCellFactory(param -> createEducationColumnDateTableCell());
        educationColumnDate.setOnEditCommit(this::setEducationDate);

        educationColumnCertification.setCellFactory(param -> new EducationCertificateColumnTableCell());

        educationColumnCertificationID.setCellFactory(model -> new EducationCertificateIdColumnTableCell());
    }

    private void initExperienceTable() {
        experienceTable.setItems(experiences);
        experienceTable.setEditable(true);

        final Button addNewRowButton = new Button(ADD_NEW_ROW_BUTTON_LABEL);
        addNewRowButton.setOnMouseClicked(
                event -> experiences.add(new Experience(employee, DateUtil.getCurrentDate())));

        experienceColumnAction.setSortable(false);
        experienceColumnAction.setGraphic(addNewRowButton);
        experienceColumnAction.setCellFactory(
                model -> new TableColumnActionTableCell(deleteColumnExperienceRowAction()));

        experienceColumnPosition.setCellFactory(param -> new ExperiencePositionColumnTableCell());

        experienceColumnDateFrom.setCellFactory(param -> createExperienceDateFromColumnTableCell());
        experienceColumnDateFrom.setOnEditCommit(this::setExperienceColumnDataFrom);

        experienceColumnDateTo.setCellFactory(param -> createExperienceDateToColumnTableCell());
        experienceColumnDateTo.setOnEditCommit(this::setExperienceColumnDataTo);
    }

    private void setEducationDate(TableColumn.CellEditEvent<Education, Date> event) {
        final Education education = initializeTableRow(event.getTableView(), event.getTablePosition().getRow());
        education.setDate(event.getNewValue());
    }

    private DateColumnTableCell<Education> createEducationColumnDateTableCell() {
        return new DateColumnTableCell(index -> educations.get((Integer) index).getDate());
    }

    private Consumer<Integer> revolveEducationDeleteRowAction() {
        return index -> educationService.delete(educations.get(index));
    }

    private DateColumnTableCell<Experience> createExperienceDateFromColumnTableCell() {
        return new DateColumnTableCell(
                index -> experiences.get((Integer) index).getDateFrom(),
                initializeDateFromActiveDatesFilter()
        );
    }

    private void setExperienceColumnDataTo(TableColumn.CellEditEvent<Experience, Date> event) {
        final Experience experience = initializeTableRow(event.getTableView(), event.getTablePosition().getRow());
        experience.setDateTo(event.getNewValue());
    }

    private BiFunction<Integer, Date, Boolean> initializeDateFromActiveDatesFilter() {
        return (index, date) -> date.before(experiences.get(index).getDateTo());
    }

    private DateColumnTableCell<Experience> createExperienceDateToColumnTableCell() {
        return new DateColumnTableCell(
                index -> experiences.get((Integer) index).getDateTo(),
                initializeDateToActiveDatesFilter()
        );
    }

    private BiFunction<Integer, Date, Boolean> initializeDateToActiveDatesFilter() {
        return (index, date) -> date.after(experiences.get(index).getDateFrom());
    }

    private void setExperienceColumnDataFrom(TableColumn.CellEditEvent<Experience, Date> event) {
        final Experience experience = initializeTableRow(event.getTableView(), event.getTablePosition().getRow());
        experience.setDateFrom(event.getNewValue());
    }

    private <T> T initializeTableRow(TableView<T> tableView, int position) {
        return tableView.getItems().get(position);
    }

    private Consumer<Integer> deleteColumnExperienceRowAction() {
        return index -> experienceService.delete(experiences.get(index));
    }

    private void initializePreparedCVDialog() {
        final Alert dialogWindow = new Alert(Alert.AlertType.NONE, EMPTY_TEXT);

        final ButtonType preparedCVButton = new ButtonType(PREPARED_CV_BUTTON, ButtonBar.ButtonData.APPLY);
        final ButtonType cancelCVButton = new ButtonType(CANCEL_BUTTON, ButtonBar.ButtonData.CANCEL_CLOSE);

        final CheckBox includeEducation = new CheckBox(INCLUDE_EDUCATION_TEXT);
        final CheckBox includeExperience = new CheckBox(INCLUDE_EXPERIENCE_TEXT);

        dialogWindow.getButtonTypes().clear();
        dialogWindow.getButtonTypes().addAll(preparedCVButton, cancelCVButton);
        dialogWindow.getDialogPane().setPrefWidth(DIALOG_WINDOWS_WIDTH);

        final VBox box = new VBox(includeEducation, includeExperience);
        box.setSpacing(VBOX_SPACING);
        dialogWindow.setGraphic(box);

        showCVDialog(preparedCVButton, dialogWindow, includeEducation, includeExperience);
    }

    private void showCVDialog(ButtonType applyButton,
                              Alert alert,
                              CheckBox educationCheckBox,
                              CheckBox experienceCheckBox) {
        alert.showAndWait().ifPresent(
                type -> preparedCVEmployee(type, applyButton, educationCheckBox, experienceCheckBox));
    }

    private void preparedCVEmployee(ButtonType type,
                                    ButtonType applyButton,
                                    CheckBox educationCheckBox,
                                    CheckBox experienceCheckBox) {
        if (type == applyButton) {
            final boolean appendEducation = educationCheckBox.isSelected();
            final boolean appendExperience = experienceCheckBox.isSelected();
            final String fileName = String.format(DEFAULT_CV_FILE_NAME,
                    EntityUtil.getEmployeeFullName(employee, DIVIDER));
            final String description = String.format(FILE_CHOOSER_DESCRIPTION, DateUtil.getCurrentDate().toString());
            final File savePath = FileChooserUtil.getFilePath(dialog.getScene().getWindow(),
                    fileName,
                    description,
                    Collections.singletonList(EXTENSIONS));

            if (savePath != null) {
                new Thread(new BuildCVEmployee(employee, appendExperience, appendEducation, savePath)).start();
            }
        }
    }
}
