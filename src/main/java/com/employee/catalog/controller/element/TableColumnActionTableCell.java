package com.employee.catalog.controller.element;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

import java.util.function.Consumer;

public class TableColumnActionTableCell<T> extends TableCell<T, Object> {

    private static final String REMOVE_BUTTON_LABEL = "-";

    private final Consumer<Integer> onDeleteAction;

    public TableColumnActionTableCell(Consumer<Integer> onDeleteAction) {
        this.onDeleteAction = onDeleteAction;
    }

    @Override
    protected void updateItem(Object obj, boolean empty) {
        super.updateItem(obj, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Button removeRowButton = new Button(REMOVE_BUTTON_LABEL);

        removeRowButton.setOnAction(event -> removeTableRow());
        setGraphic(removeRowButton);
    }

    private void removeTableRow() {
        onDeleteAction.accept(getIndex());
        getTableView().getItems().remove(getIndex());
    }
}
