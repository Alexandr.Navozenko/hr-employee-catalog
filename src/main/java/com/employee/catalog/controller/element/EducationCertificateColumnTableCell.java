package com.employee.catalog.controller.element;

import com.employee.catalog.domain.Education;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import org.apache.commons.lang3.StringUtils;

public class EducationCertificateColumnTableCell extends TableCell<Education, Boolean> {

    @Override
    protected void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Education education = getTableView().getItems().get(getIndex());
        final CheckBox hasCertificateCheckBox = new CheckBox();
        boolean hasCertificate = !StringUtils.isBlank(education.getCertificate());

        hasCertificateCheckBox.setSelected(hasCertificate);

        hasCertificateCheckBox.setOnAction(event -> updateIsCertificated(education, hasCertificateCheckBox));

        setGraphic(hasCertificateCheckBox);
    }

    private void updateIsCertificated(Education education, CheckBox checkBox) {
        education.getCertificated().set(checkBox.isSelected());
    }
}
