package com.employee.catalog.controller.element;

import com.employee.catalog.domain.Education;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

public class EducationCertificateIdColumnTableCell extends TableCell<Education, String> {

    private static final String EMPTY_TEXT = "";

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Education education = getTableView().getItems().get(getIndex());
        final TextField certificateIdTextField = new TextField();

        certificateIdTextField.editableProperty().bind(education.getCertificated());
        education.getCertificated().set(education.getCertificate() != null);
        certificateIdTextField.setText(getTableView().getItems().get(getIndex()).getCertificate());

        education.getCertificated().addListener(
                (observable, oldValue, newValue) -> setEmptyCertificateID(certificateIdTextField, newValue));

        certificateIdTextField.textProperty().addListener(
                (observable, oldValue, newValue) -> updateCertificateId(education, newValue));

        setGraphic(certificateIdTextField);
    }

    private void updateCertificateId(Education education, String newValue) {
        education.setCertificate(newValue);
        commitEdit(newValue);
    }

    private void setEmptyCertificateID(TextField textField, boolean newValue) {
        if (!newValue) {
            textField.setText(EMPTY_TEXT);
        }
    }
}
