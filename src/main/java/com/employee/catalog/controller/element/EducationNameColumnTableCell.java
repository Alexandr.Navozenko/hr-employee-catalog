package com.employee.catalog.controller.element;

import com.employee.catalog.domain.Education;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

public class EducationNameColumnTableCell extends TableCell<Education, String> {

    private static final String DEFAULT_PROMPT_TEXT = "required field";

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Education education = getTableView().getItems().get(getIndex());
        final TextField educationNameTextField = new TextField();

        educationNameTextField.setPromptText(DEFAULT_PROMPT_TEXT);
        educationNameTextField.setText(getTableView().getItems().get(getIndex()).getEducationName());
        educationNameTextField.textProperty().addListener(
                (observable, oldValue, newValue) -> updateEducationName(education, newValue));

        setGraphic(educationNameTextField);
    }

    private void updateEducationName(Education education, String newValue) {
        education.setEducationName(newValue);
        commitEdit(newValue);
    }
}
