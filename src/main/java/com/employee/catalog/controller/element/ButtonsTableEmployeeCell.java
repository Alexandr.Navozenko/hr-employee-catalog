package com.employee.catalog.controller.element;

import com.employee.catalog.common.EmployeeView;
import com.employee.catalog.utils.UIComponentUtil;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

@Slf4j
public class ButtonsTableEmployeeCell extends TableCell<EmployeeView, Button> {

    private static final String FONT_AWESOME_ICON_EDIT = "EDIT";

    private static final String FONT_AWESOME_ICON_TRASH = "TRASH";

    private static final String REMOVE_ALERT_MSG = "Are you sure, you want to remove the employee?";

    private static final String ALERT_TITLE = "Delete Employee?";

    private static final double HBOX_SPACING = 10.0;

    private final Consumer<Integer> onDeleteEmployeeAction;

    private final Consumer<Integer> onShowEmployeeFormAction;

    public ButtonsTableEmployeeCell(Consumer<Integer> onShowEmployeeFormAction,
                                    Consumer<Integer> onDeleteEmployeeAction) {
        this.onShowEmployeeFormAction = onShowEmployeeFormAction;
        this.onDeleteEmployeeAction = onDeleteEmployeeAction;
    }

    @Override
    protected void updateItem(Button button, boolean empty) {
        super.updateItem(button, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Button editEmployeeBtn = initializeEditButton();
        final Button deleteEmployeeBtn = initializeDeleteButton();
        final HBox hBox = createHBoxWithButtons(editEmployeeBtn, deleteEmployeeBtn);

        setGraphic(hBox);
    }

    private Button initializeEditButton() {
        final Button editButton = buildActionButton(FONT_AWESOME_ICON_EDIT);

        editButton.setOnAction(event -> showSelectedEmployeeForm());

        return editButton;
    }

    private void showSelectedEmployeeForm() {
        onShowEmployeeFormAction.accept(getIndex());
    }

    private Button initializeDeleteButton() {
        final Button deleteButton = buildActionButton(FONT_AWESOME_ICON_TRASH);

        deleteButton.setOnAction(event -> deleteSelectedEmployee());

        return deleteButton;
    }

    private void deleteSelectedEmployee() {
        final Alert confirmationAlert = UIComponentUtil.initializeConfirmationAlert(REMOVE_ALERT_MSG, ALERT_TITLE);

        confirmationAlert.showAndWait();

        if (confirmationAlert.getResult() == ButtonType.OK) {
            onDeleteEmployeeAction.accept(getIndex());
            getTableView().getItems().remove(getIndex());
        }
    }

    private Button buildActionButton(String fontAwesomeIconName) {
        final Button button = new Button();
        final ImageView buttonImage = new ImageView(UIComponentUtil.initializeButtonImage(fontAwesomeIconName));

        button.setGraphic(buttonImage);

        return button;
    }

    private HBox createHBoxWithButtons(Button editEmployeeButton, Button deleteEmployeeButton) {
        final HBox hBox = new HBox(editEmployeeButton, deleteEmployeeButton);

        hBox.setSpacing(HBOX_SPACING);
        hBox.setAlignment(Pos.CENTER);

        return hBox;
    }
}
