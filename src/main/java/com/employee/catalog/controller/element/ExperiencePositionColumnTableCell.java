package com.employee.catalog.controller.element;

import com.employee.catalog.domain.Experience;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

public class ExperiencePositionColumnTableCell  extends TableCell<Experience, String> {

    private static final String DEFAULT_PROMPT_TEXT = "required field";

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Experience experience = getTableView().getItems().get(getIndex());
        final TextField positionNameTextField = new TextField();

        positionNameTextField.setPromptText(DEFAULT_PROMPT_TEXT);
        positionNameTextField.setText(getTableView().getItems().get(getIndex()).getPosition());
        positionNameTextField.textProperty().addListener(
                (observable, oldValue, newValue) -> updateExperiencePosition(experience, newValue));

        setGraphic(positionNameTextField);
    }

    private void updateExperiencePosition(Experience experience, String newValue) {
        experience.setPosition(newValue);
        commitEdit(newValue);
    }
}
