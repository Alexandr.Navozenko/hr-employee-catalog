package com.employee.catalog.controller.element;

import com.employee.catalog.utils.DateUtil;
import javafx.event.Event;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.function.BiFunction;
import java.util.function.Function;

public class DateColumnTableCell<T> extends TableCell<T, Date> {

    private final BiFunction<Integer, Date, Boolean> filter;
    private final Function<Integer, Date> cellValueDate;

    private DatePicker datePicker;

    public DateColumnTableCell(Function<Integer, Date> cellValueDate, BiFunction<Integer, Date, Boolean> filter) {
        this.cellValueDate = cellValueDate;
        this.filter = filter;
    }

    public DateColumnTableCell(Function<Integer, Date> modelDate) {
        this.cellValueDate = modelDate;
        this.filter = (index, date) -> true;
    }

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();

            setText(null);
            setGraphic(datePicker);
        }
    }

    @Override
    public void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);

        setText(null);

        if (empty) {
            setGraphic(null);
        } else {
            initializeDatePicker();
            setGraphic(datePicker);
        }
    }

    private void initializeDatePicker() {
        datePicker = new DatePicker(getDate().toLocalDate());
        datePicker.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        datePicker.setDayCellFactory(picker -> createDateCell());
        datePicker.setOnAction(event -> fireCustomEditEvent(convertDate(datePicker.getValue())));
    }

    private DateCell createDateCell() {
        return new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || !filter.apply(getIndex(),
                        convertDate(date)) || DateUtil.getCurrentDate().before(convertDate(date)));
            }
        };
    }

    private Date convertDate(LocalDate date) {
        final long convertedDate = date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();

        return new Date(convertedDate);
    }

    private void fireCustomEditEvent(Date value) {
        final TableColumn.CellEditEvent<T, Date> editEvent =
                new TableColumn.CellEditEvent<>(getTableView(),
                        new TablePosition<>(getTableView(), getIndex(), getTableColumn()),
                        TableColumn.editCommitEvent(),
                        value
                );
        Event.fireEvent(getTableColumn(), editEvent);
    }

    private Date getDate() {
        final Date date = cellValueDate.apply(getIndex());

        return date == null ? DateUtil.getCurrentDate() : date;
    }
}
