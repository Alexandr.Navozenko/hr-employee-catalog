package com.employee.catalog.controller;

import com.employee.catalog.component.PrimaryStageInitializer;
import com.employee.catalog.controller.element.ButtonsTableEmployeeCell;
import com.employee.catalog.common.EmployeeView;
import com.employee.catalog.domain.Employee;
import com.employee.catalog.service.EmployeeService;
import com.employee.catalog.utils.EntityUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import net.rgielen.fxweaver.core.FxControllerAndView;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
@Component
@FxmlView("EmployeeList.fxml")
public class EmployeeListController {

    private Stage stage;

    private ObservableList<EmployeeView> employers;

    private final EmployeeService employeeService;

    private static final String REMOVE_LOG_MSG = "Employee %s removed";

    private static final String DIVIDER = " ";

    @FXML
    private TableView<EmployeeView> employeeTable;

    @FXML
    private TableColumn<EmployeeView, String> columnName;

    @FXML
    private TableColumn<EmployeeView, String> columnDepartment;

    @FXML
    private TableColumn<EmployeeView, String> columnPosition;

    @FXML
    private TableColumn<EmployeeView, Button> columnAction;

    private final FxControllerAndView<EmployeeFormController, AnchorPane> employeeForm;

    @Autowired
    private PrimaryStageInitializer primaryStageInitializer;

    @Autowired
    public EmployeeListController(FxControllerAndView<EmployeeFormController, AnchorPane> employeeForm,
                                  EmployeeService employeeService) {
        this.employeeForm = employeeForm;
        this.employeeService = employeeService;
    }

    @FXML
    void initialize() {
        if (stage == null) {
            this.stage = primaryStageInitializer.getPrimaryStage();

            initTable();
        }
    }

    @FXML
    void switchToEmployeeForm(ActionEvent event) {
        employeeForm.getController().showAndWait(null);
    }

    public void initTable() {
        employers = FXCollections.observableArrayList();
        employers.clear();

        employers.addAll(getEmployeeViews());
        employeeTable.setItems(employers);

        initColumns();
    }

    public void refreshTableView() {
        employers.clear();
        employers.addAll(getEmployeeViews());
    }

    private List<EmployeeView> getEmployeeViews() {
        return employeeService.findAll().stream()
                .sorted(Comparator.comparing(Employee::getId))
                .map(EmployeeView::new)
                .collect(Collectors.toList());
    }

    private void initColumns() {
        columnName.setCellValueFactory(view -> view.getValue().getFullName());
        columnDepartment.setCellValueFactory(view -> view.getValue().getDepartment());
        columnPosition.setCellValueFactory(view -> view.getValue().getPosition());
        columnAction.setCellFactory(
                value -> new ButtonsTableEmployeeCell(showSelectedEmployeeForm(), deleteColumnEmployeeRowAction()));
    }

    private Consumer<Integer> deleteColumnEmployeeRowAction() {
        return index -> {
            final Employee currentEmployee = employers.get(index).getEmployee();
            employeeService.delete(currentEmployee);
            log.info(String.format(REMOVE_LOG_MSG, EntityUtil.getEmployeeFullName(currentEmployee, DIVIDER)));
        };
    }

    private Consumer<Integer> showSelectedEmployeeForm() {
        return index -> {
            final EmployeeView employeeView = employers.get(index);
            employeeForm.getController().showAndWait(employeeView.getEmployee());
        };
    }
}
