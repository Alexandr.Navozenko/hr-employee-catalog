package com.employee.catalog.model;

import com.employee.catalog.domain.Department;
import com.employee.catalog.domain.Education;
import com.employee.catalog.domain.Employee;
import com.employee.catalog.domain.Experience;
import com.employee.catalog.domain.Position;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Data
public class EmployeeReportModel {
    private Integer id;

    private String firstName;

    private String lastName;

    private Department department;

    private Position position;

    private List<Experience> experiences;

    private List<Education> educations;

    public EmployeeReportModel(Employee employee,
                               Set<Experience> experiences,
                               Set<Education> educations) {
        this.id = employee.getId();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.position = employee.getPosition();
        this.department = position.getDepartment();
        this.experiences = new ArrayList<>(experiences);
        this.educations = new ArrayList<>(educations);
    }
}
