package com.employee.catalog.service.impl;

import com.employee.catalog.repository.ExperienceRepository;
import com.employee.catalog.domain.Experience;
import com.employee.catalog.service.ExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExperienceServiceImpl implements ExperienceService {

    private final ExperienceRepository experienceRepository;

    @Autowired
    public ExperienceServiceImpl(ExperienceRepository experienceRepository) {
        this.experienceRepository = experienceRepository;
    }

    @Override
    public void delete(Experience experience) {
        experienceRepository.delete(experience);
    }
}
