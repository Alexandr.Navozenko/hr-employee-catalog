package com.employee.catalog.service.impl;

import com.employee.catalog.repository.DepartmentRepository;
import com.employee.catalog.domain.Department;
import com.employee.catalog.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Department findByName(String name) {
        return departmentRepository.findByName(name);
    }

    @Override
    public List<String> getDepartmentNames() {
        List<Department> departments = departmentRepository.findAll();
        return departments.stream()
                .map(department -> department.getName())
                .collect(Collectors.toList());
    }
}
