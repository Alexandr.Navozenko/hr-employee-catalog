package com.employee.catalog.service.impl;

import com.employee.catalog.repository.EducationRepository;
import com.employee.catalog.domain.Education;
import com.employee.catalog.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EducationServiceImpl implements EducationService {
    private final EducationRepository educationRepository;

    @Autowired
    public EducationServiceImpl(EducationRepository educationRepository) {
        this.educationRepository = educationRepository;
    }

    @Override
    public void delete(Education education) {
        educationRepository.delete(education);
    }
}
