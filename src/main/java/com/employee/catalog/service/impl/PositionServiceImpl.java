package com.employee.catalog.service.impl;

import com.employee.catalog.repository.PositionRepository;
import com.employee.catalog.service.PositionService;
import com.employee.catalog.domain.Department;
import com.employee.catalog.domain.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PositionServiceImpl implements PositionService {

    private final PositionRepository positionRepository;

    @Autowired
    public PositionServiceImpl(PositionRepository positionRepository) {
        this.positionRepository = positionRepository;
    }

    @Override
    public Position findByName(String name) {
        return positionRepository.findByName(name);
    }

    @Override
    public List<String> getPositionNames(Department department) {
        List<Position> positions = positionRepository.findByDepartmentId(department.getId());

        return positions.stream()
                .map(Position::getName)
                .collect(Collectors.toList());
    }
}
