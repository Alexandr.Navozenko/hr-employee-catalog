package com.employee.catalog.service;

import com.employee.catalog.domain.Department;

import java.util.List;

public interface DepartmentService {
    Department findByName(String name);

    List<String> getDepartmentNames();
}
