package com.employee.catalog.service;

import com.employee.catalog.domain.Department;
import com.employee.catalog.domain.Position;

import java.util.List;

public interface PositionService {
    Position findByName(String name);

    List<String> getPositionNames(Department department);
}
