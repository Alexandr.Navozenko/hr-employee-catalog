package com.employee.catalog.service;

import com.employee.catalog.domain.Experience;

public interface ExperienceService {
    public void delete(Experience experience);
}
