package com.employee.catalog.service;

import com.employee.catalog.domain.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAll();

    Employee save(Employee employee);

    void delete(Employee employee);
}
