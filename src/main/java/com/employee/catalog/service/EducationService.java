package com.employee.catalog.service;

import com.employee.catalog.domain.Education;

public interface EducationService {
    void delete(Education education);
}
